# Written by Pippa Crawshaw
from sys import argv
from graph_pie_builder import PieChartBuilder
from graph_bar_builder import BarChartBuilder
from graph_director import GraphDirector
from file_types import FileTypes
import os


class Controller(object):
    _input_file = ''
    _output_file = ''
    _data_list = []

    def __init__(self, command_view, model):
        """
        Initialises the views and model
        :param command_view: the cmd view
        :param model: the employee model
        """
        self.command_view = command_view
        self.model = model

    def start(self):  # pragma: no cover
        """
        Checks to see if line orientated or command line
        """
        if len(argv) > 1:
            self.command_view.onecmd(' '.join(argv[1:]))
        else:
            self.command_view.cmdloop()

    def open_file_and_validate(self, file_name):
        """
        Checks to make sure it is a valid file
        If it is a valid file it open the file and puts it into a list
        Validates the records and fields in the file
        Drops any invalid records and returns an error list if necessary
        :param file_name:
        :return: error list
        """

        if self.get_list_of_data_objects(file_name):
            # send the input to be validated
            self._data_list, error_list = self.model.employee_info(self._data_list)
            if not error_list:
                print(file_name, "Inputted with no errors")
            else:
                print("Records with errors dropped:")

            # print out errors that caused the records to be dropped
            self._input_file.output(error_list)

    def show_graph(self, file_name, the_type):
        """
        Shows a graph of the gender types
        :param file_name:
        :param the_type:
            default value bar_chart
            possible values: pie, bar_chart
        :return: plt graph object
        """
        _graph = None
        graph_director = GraphDirector(None)

        if self.get_list_of_data_objects(file_name):
            # sends data off to a graph object
            if the_type == "pie":
                graph_builder = PieChartBuilder(self._data_list)
                graph_director.set_builder(graph_builder)
            else:
                graph_builder = BarChartBuilder(self._data_list)
                graph_director.set_builder(graph_builder)

            graph_director.construct()
            _graph = graph_builder.get_graph()
        return _graph

    def save_data_to_new_file(self, file_name):
        """
        Checks that the filename is not already in use
        Validates the extension as a valid type
        Saves the data to the new file name
        :param file_name: format - file_name.extension
            valid extensions - .csv, .db, .pickle
        :return:
        """
        succeed = False
        if self._data_list:
            succeed = self.save_data_if_valid_file(file_name)
        else:
            print("There is no data present. "
                  "Use 'file <filename>' to input data")
        return succeed

    def save_data_if_valid_file(self, file_name):
        succeed = False
        if self.check_file_exists(file_name):
            print("File already exists, choose a different file name.")
        else:
            if self.check_file_name_extensions(file_name, 'output'):
                # do the actual saving
                self._output_file.save_data_to_new(file_name, self._data_list)
                succeed = True
        return succeed

    def show_records(self):
        result = False
        for i in self._data_list:
            result = True
            print(i)
        return result

    def get_list_of_data_objects(self, file_name):
        if self.check_valid_file_name(file_name, 'input'):
            self._data_list = self._input_file.get_input(file_name)
            return True
        else:
            return False

    def check_valid_file_name(self, file_name, input_output):
        """
        Checks to make sure the file_name exists and if it
        has a valid file extension
        :param file_name:
        :param input_output:
        :return:
        """
        if self.check_file_exists(file_name):
            if self.check_file_name_extensions(file_name, input_output):
                return True
            else:
                return False
        else:
            print("File does not exist")
            return False

    def check_file_name_extensions(self, file_name, input_output):
        """
        Checks the file extension against the possible extensions set up
        in the class variable _extension_types
        :param file_name:
        :param input_output:
        :return:
        """
        file_type = FileTypes()
        extension_types = file_type.get_extension_types()
        for extension in extension_types:
            if file_name.endswith(extension):
                if input_output == 'input':
                    self._input_file = file_type.get_file_type(extension)
                else:
                    self._output_file = file_type.get_file_type(extension)
                return True
        print("File name must end with:")
        for extension in extension_types:
            print(extension)
        return False

    @staticmethod
    def check_file_exists(file_name):
        """
        Checks to make sure the file exists
        :param file_name:
        :return:
        """
        if os.path.isfile(file_name):
            return True
        else:
            return False
