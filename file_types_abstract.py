# written by Pippa Crawshaw - June 2017

from abc import ABCMeta, abstractmethod
from file_csv import CsvFile
from file_database import DatabaseFile
from file_pickle import PickleFile


class FileTypesAbstract(metaclass=ABCMeta):
    def __init__(self):
        self.file_types = {"csv": CsvFile(),
                      "db": DatabaseFile(),
                      "pickle": PickleFile()}

    @abstractmethod
    def create_file_type(self, extension):
        pass

    @abstractmethod
    def get_file_type(self):
        pass
