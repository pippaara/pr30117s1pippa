# written by Pippa Crawshaw - March 2017

# not completed

import shelve
from data import View

class ShelveData(View):
    def __init__(self):
        pass

    def get_input(self, file_name):
        with shelve.open(file_name) as s:
            existing = s['myDict']
        return self.dict_to_list(existing)

    def save_data_to_new(self, file_name, data_list):
        with shelve.open(file_name) as my_shelfed_dict:
            my_shelfed_dict["myDict"] = self.list_to_dict(data_list)

    # def list_to_dict(self, li):
    #     dct = {}
    #     count = 0;
    #     for item in li:
    #         dct[count] = item
    #         count += 1
    #     return dct
    #
    # def dict_to_list(self, dict):
    #     list_values = [v for v in dict.values()]
    #     return list_values