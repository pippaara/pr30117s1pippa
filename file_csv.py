# written by Pippa Crawshaw - March 2017
"""
input and output of a csv file
"""

from file_abstract import File
import csv


class CsvFile(File):
    @staticmethod
    def get_input(file_name):
        """
        Reads the file and returns it's contents as a list
        :param file_name:
        :return:
        """
        read_csv = []
        try:
            with open(file_name, newline='') as incsv:
                # the csv file is converted to lists of lists
                # csv.reader does not retain information once the file is closed
                # fields are placed into list as strings
                read_csv = list(csv.reader(incsv, delimiter=','))
        except Exception as e:
            print(e)
            return False
        else:
            print(file_name, "opened successfully")
        return read_csv


    @staticmethod
    def save_data_to_new(file_name, data_list):
        """
        Saves the data from <data_list> into a new csv file named <file_name>
        :param file_name:
        :param data_list:
        :return:
        """
        with open(file_name, "a") as outcsv:
            writer = csv.writer(outcsv, delimiter=',')
            # write header line to outcsv
            writer.writerow(['emp_id', 'gender', 'age',
                             'sales', 'bmi', 'salary', 'dob'])
            for line in data_list:
                # write line to outcsv
                writer.writerows([line, ])
