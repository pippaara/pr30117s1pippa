# written by Pippa Crawshaw - March 2017

from controller import Controller
from employee import Employee
from command_view import CommandView


if __name__ == "__main__":
    cmd_view = CommandView()
    # running controller function
    ctrl = Controller( cmd_view, Employee())
    # set the controller so cmd_view can call methods within the controller
    cmd_view.set_controller(ctrl)
    ctrl.start()


