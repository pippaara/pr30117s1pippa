# Created by Simon Winder

from validate_field import ValidateField
import re


class Sales(ValidateField):
    def validate(self):
        # PC corrected to {3} not {2,3} 22-03-17
        if re.match('^[0-9]{3}$', self._field) is not None:
            return self._valid
        else:
            return 'Sales must be 3 numbers'
