# Created by Simon Winder


from abc import abstractmethod, ABCMeta

from six import add_metaclass

@add_metaclass(ABCMeta)
class AgeValidate:
    @abstractmethod
    def check_age_against_date(self, age):
        pass